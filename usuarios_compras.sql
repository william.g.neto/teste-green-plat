/* Criando banco */
CREATE DATABASE greenplat;
USE greenplat;

/* Criando e inserindo dados nas tabelas */
CREATE TABLE compras (
    id int,
    id_usuario int,
    valor int,
    item varchar(255)
);

INSERT INTO compras VALUES (
    1,1,500,"Colchão"
),(
    2,2,250,"Jogo de Pratos"
), (
    3,2,1000,"Celular"
);

CREATE TABLE usuarios (
    id int,
    nome varchar(255),
    email varchar(255)
);

INSERT INTO usuarios VALUES (
    1, "João", "joao@email.com.br"
), (
    2, "Pedro", "pedro@email.com.br"
),(
    3, "Maria", "maria@email.com.br"
);

/* Selecionando compras sem utilizar join */
SELECT id_usuario, nome,compras.id as id_compra, valor as valor_compra, item as item_compra from usuarios, compras WHERE compras.id_usuario=usuarios.id;

/* Selecionando compras utilizando join */
SELECT usuarios.id as id_usuario, usuarios.nome, compras.id as id_compra, compras.valor as valor_compra, compras.item as item_compra
FROM usuarios
LEFT JOIN compras ON compras.id_usuario=usuarios.id;