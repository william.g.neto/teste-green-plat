from flask import Flask, request, Response, jsonify
from users import Users

app = Flask(__name__)
users_model = Users()

# Receber nome de usuário,
# salvando nome e id em arquivo json
@app.route("/add/", methods=["POST"])
def add_user():
    username = request.json.get("username")
    if not username:
        return jsonify({"msg": "Informe o campo username"}), 405

    users_model.create_user(username)

    return jsonify({"msg": "Usuário criado com sucesso"}), 201

# Retornar os ids e nomes de todos
# os usuários salvos
@app.route("/list/", methods=["GET"])
def list_users():
    return jsonify(users_model.list_users())

# Alterar nome de um usuário cadastrado
@app.route("/update/", methods=["PUT"])
def update_user():
    user_id = request.json.get("user_id")
    username = request.json.get("username")

    if not user_id:
        return jsonify({"msg": "Informe o campo user_id"}), 405
    
    if not username:
        return jsonify({"msg": "Informe o parâmetro username"}), 405
    
    users_model.update_user(
        user_id,
        username
    )

    return jsonify({"msg": "Usuário atualizado com sucesso"}), 200

# Remover um usuário
@app.route("/delete/", methods=["delete"])
def delete_user():
    user_id = request.json.get("user_id")

    if not user_id:
        return jsonify({"msg": "Informe o campo user id"}), 405

    users_model.delete_user(user_id)
    return jsonify({"msg": "Usuário removido com sucesso"}), 200
