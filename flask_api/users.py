import uuid, json

class Users():
    db = "usersdb.json"

    def get_db_users(self):
        with open(self.db, "r") as dbfile:
            users = json.load(dbfile)
        
        return users
    
    def save_db_users(self, users):
        with open(self.db, "w") as dbfile:
            json.dump(
                users,
                dbfile
            )

    def create_user(self, name):
        users = self.get_db_users()
        users[str(uuid.uuid4())] = name

        self.save_db_users(users)

    
    def list_users(self):
        print(self.get_db_users())
        return self.get_db_users()
    
    def update_user(self, id, name):
        users = self.get_db_users()
        users[id] = name

        self.save_db_users(users)
    
    def delete_user(self, id):
        users = self.get_db_users()
        del users[id]

        self.save_db_users(users)