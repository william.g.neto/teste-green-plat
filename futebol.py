
class Time():
    def __init__(
        self, 
        pontos: int, 
        gols_pro: int, 
        gols_contra: int, 
        partidas: int
    ):
        self.pontos = pontos
        self.gols_pro = gols_pro
        self.gols_contra = gols_contra
        self.partidas = partidas

    # Calcular a média de gols por partida
    def media_gols(self):
        return self.gols_pro / self.partidas
    
    # Calcular a média de pontos por partida
    def media_pontos(self):
        return self.pontos / self.partidas

    # Calcular o saldo de gols
    def saldo_gols(self):
        return self.gols_pro - self.gols_contra
    
    # Atualizar os atributos de acordo com resultados
    # de uma partida
    def nova_partida(
        self,
        resultado: str,
        gols_pro: int,
        gols_contra: int
    ):
        # vitória - 3 pontos
        # empate - 1 ponto
        # derota - 0 pontos

        pontos = 0
        if resultado == "vitória":
            pontos = 3
        elif resultado == "empate":
            pontos = 1
        
        self.pontos += pontos
        self.partidas += 1
        self.gols_pro += gols_pro
        self.gols_contra += gols_contra
    
    # Retornar status do time
    def status_time(self):
        return {
            "partidas jogadas": self.partidas,
            "Pontos feitos": self.pontos,
            "Saldo de gols": self.saldo_gols(),
            "Média de gols por partida": self.media_gols(),
            "Média de pontos por partida": self.media_pontos()
        }
